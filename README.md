# Alexandre_Adrien_IA

~~Pour le lancer~~

    - lancer le server
    - lancer un client investigator, le votre ou le notre avec "python3 investigator.py"
    - lancer un client fantom, le votre ou le notre avec "python3 fantom.py"

~~Comment ça marche~~

1) Notre programme (investigator et fantom) récupère une question du server avec les données du jeu.
2) On construit un arbre avec tout les choix possible qui en découlle jusqu'a qu'il n'y est plus de personnage a choisir.
*Pour ne pas surcharger l'arbre plus que nécessaire on ne crée pas de node fille pour le changement de shadow dans la meme pièce que là ou elle ce trouve dejà, on ne presente pas les doublons pour le pouvoir du personnages bleu (blocked) et les personnages pouvant jouer leur pouvoir avant ou apres leur deplacement ne peuvent le faire qu'avant dans notre arbres*
3) On donne l'arbre à une fonction Minimax alpha beta
4) Notre minimax appelle une fonction heuristic pour les dernieres nodes
5) Notre minimax retourne finalement le meilleur choix !

~~Heuristique~~

- On sépare toujours le suspects en deux groupes :
        -> ceux isolés (ou se trouvant dans la salle sombre) qui crieront à la fin du tour si le fantôme est parmis eux
        -> ceux en groupes, et dont aucun ne criera même si le fantôme en fait partie

On sépare ensuite l'heuristique selon le cas du fantôme ou de l'inspecteur.

Dans le cas du fantôme :
    La valeur de l'heuristique correspond au nombre de suspects se trouvant dans le même cas de figure que le fantôme. Plus il y a de suspects dans ce cas de figure, plus la valeur retournée est élevée. (Elle ira entre 0 et 8)

Dans le cas de l'inspecteur:
    La valeur de l'heuristique correspond au nombre minimum de suspects que l'inspecteur éliminera à la fin du tour. Moins il y a d'écart entre les deux groupes (dont le total correspond à l'intégralité des suspects), plus la valeur retournée sera élevée. (Elle ira entre 0 et 4)


~~Exemple :~~

1) node en profondeur 0-----------select character : (white)-------------
2) lien ----------------------------------------|
3) nodes en profondeur 1--------------select position (1, 4)
4) lien ----------------------------------------|
5) nodes en profondeur 2---activate power (0, 1)---activate power (0, 1)
6) lien -------------------------|---------------------------|
7) nodes en profondeur 3-----ETC---ETC-------------------ETC---ETC

en gros on crée une node pour chaque potentielle question que nous pose le serveur et on sauvegarde l'arbre pour ne pas avoir a le reconstruire a chaque fois, on ne le reconstruis que si la question reçus est 'select character'.
