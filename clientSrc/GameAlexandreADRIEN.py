
class GameAlexandreAdrien :

    def __init__(self):
        """
        Initiation
        Parameters
        ----------
        color    : string
            color of the current player (none if select character)
        position : int
            position of the current player (none if select character)
        passage  : list
            basic passage in the map
        """
        self.passages = [{1, 4}, {0, 2}, {1, 3}, {2, 7}, {0, 5, 8},
            {4, 6}, {5, 7}, {3, 6, 9}, {4, 9}, {7, 8}]
        self.pink_passages = [{1, 4}, {0, 2, 5, 7}, {1, 3, 6}, {2, 7}, {0, 5, 8, 9},
                 {4, 6, 1, 8}, {5, 7, 2, 9}, {3, 6, 9, 1}, {4, 9, 5},
                 {7, 8, 4, 6}]
        self.permanents = {"pink"}
        self.both = {'red', 'grey', 'blue'}
        self.before = {'purple', 'brown'}
        self.after = {'black', 'white'}
        self.colors = self.before | self.permanents | self.after | self.both
        self.chosenCharacter = dict
        self.node = None

    def calculateNewData(self, data, choice):
        if (data["question type"] == "select character"):
            return self.selectCharacter(data, choice)
        elif (data["question type"] == "select position"):
            return self.selectPosition(data, choice)
        else:
            return self.selectPower(data, choice)

        return data

    def selectCharacter(self, data, choice):
        data["chosen character"]["color"] = data["data"][choice]["color"]
        data["chosen character"]["usingPower"] = False
        data["chosen character"]["alreadyUsePower"] = False
        data["chosen character"]["canMove"] = True
        for q in data["game state"]["characters"]:
            if q["color"] == data["chosen character"]["color"]:
                data["chosen character"]["position"] = q["position"]

        if data["chosen character"]["color"] == "red":
            data["question type"] = "red character power activation time"
            data["data"] = ["before", "after"]
        elif data["chosen character"]["color"] in list(self.before | self.both):
            data["question type"] = "activate " + data["chosen character"]["color"] + " power"
            data["data"] = [0, 1]

        else:
            data["question type"] = "select position"
            pass_act = self.pink_passages if data["chosen character"]["color"] == 'pink' else self.passages
            disp = {x for x in pass_act[data["chosen character"]["position"]]
                if data["chosen character"]["position"] not in data["game state"]["blocked"] or x not in data["game state"]["blocked"]}
                #POUR moi c'est censé etre un and...mais bon -.o
            data["data"] = list(disp)

        for q in data["game state"]["active character_cards"]:
            if q["color"] == data["chosen character"]["color"]:
                data["game state"]["active character_cards"].remove(q)

        return data

    def selectPosition(self, data, choice):
        # change position
        data["chosen character"]["canMove"] = False
        # for brown character active power
        if (data["chosen character"]["color"] == "brown" and data["chosen character"]["usingPower"]):
            for q in data["game state"]["characters"]:
                if q["position"] == data["chosen character"]["position"] and q["color"] != data["chosen character"]["color"]:
                    q["position"] = data["data"][choice]
        # for all player
        for q in data["game state"]["characters"]:
            if q["color"] == data["chosen character"]["color"]:
                q["position"] = data["data"][choice]
        data["chosen character"]["position"] = data["data"][choice]

        # set question and choice for activate power
        if (data["chosen character"]["alreadyUsePower"] == False and data["chosen character"]["color"] in self.after):
            data["question type"] = "activate " + data["chosen character"]["color"] + " power"
            data["data"] = [0, 1]
        # set question and choice for select character
        else:
            data["question type"] = "select character"
            data["data"] = data["game state"]["active character_cards"]

        return data


    def selectPower(self, data, choice):
        if (data["data"][choice] == 1 and data["question type"].startswith("activate ")):
            data["chosen character"]["usingPower"] = True
            data["chosen character"]["alreadyUsePower"] == True

        if (data["chosen character"]["usingPower"] == True):

            # red character
            if data["chosen character"]["color"] == "red":
                data["chosen character"]["usingPower"] = False
                ###### JE SAIS PAS  >o< !!!!

            # black character
            elif (data["chosen character"]["color"] == "black"):
                for q in data["game state"]["characters"]:
                    if q["position"] in {x for x in self.passages[data["chosen character"]["position"]] if
                                      x not in data["game state"]["blocked"]
                                      and q["position"] not in data["game state"]["blocked"]}:
                                      #POUR moi c'est censé etre un and...mais bon -.o
                        q["position"] = data["chosen character"]["position"]
                data["chosen character"]["usingPower"] = False

            # white character
            elif (data["chosen character"]["color"] == "white"):
                # move charact
                if (data["question type"].startswith("activate") == False):
                    string = data["question type"].split()
                    for q in data["game state"]["characters"]:
                        if (q["color"] == string[-1]):
                            q["position"] = data["data"][choice]
                charToMove = [c for c in data["game state"]["characters"] if c["position"] == data["chosen character"]["position"] and data["chosen character"]["color"] != c["color"]]
                if (len(charToMove) > 0):
                    disp = {
                            x for x in self.passages[data["chosen character"]["position"]]
                            if x not in data["game state"]["blocked"] or charToMove[0]["position"] not in data["game state"]["blocked"]}
                            #POUR moi c'est censé etre un and...mais bon -.o
                    # edit
                    available_positions = list(disp)

                    data["question type"] = "white character power move " + charToMove[0]["color"]
                    data["data"] = available_positions
                else:
                    data["chosen character"]["usingPower"] = False

            # purple character
            elif (data["chosen character"]["color"] == "purple"):
                data["chosen character"]["canMove"] = False
                data["chosen character"]["canMove"] = False
                available_characters = list(self.before | self.permanents | self.after | self.both)
                available_characters.remove("purple")
                if (data["question type"].startswith("activate") == True):
                    data["question type"] = "purple character power"
                    data["data"] = available_characters
                elif (data["question type"] == "purple character power"):
                    for q in data["game state"]["characters"]:
                        if q["color"] == data["data"][choice]:
                            colorPos = q["position"]
                    for q in data["game state"]["characters"]:
                        if q["color"] == "purple":
                            q["position"] = colorPos
                    for q in data["game state"]["characters"]:
                        if q["color"] == data["data"][choice]:
                            q["position"] = data["chosen character"]["position"]
                    data["chosen character"]["position"] = colorPos
                    data["chosen character"]["usingPower"] = False

            # brown character
            elif (data["chosen character"]["color"] == "brown"):
                data["chosen character"]["usingPower"] = True
                data["question type"] = "select position"
                pass_act = self.pink_passages if data["chosen character"]["color"] == 'pink' else self.passages
                disp = {x for x in pass_act[data["chosen character"]["position"]]
                    if data["chosen character"]["position"] not in data["game state"]["blocked"] or x not in data["game state"]["blocked"]}
                    #POUR moi c'est censé etre un and...mais bon -.o
                data["data"] = list(disp)

            # grey character
            if data["chosen character"]["color"] == "grey":
                available_rooms = [room for room in range(10) if room != data["game state"]["shadow"]]
                if (data["question type"].startswith("activate") == True):
                    data["question type"] = "grey character power"
                    data["data"] = available_rooms
                elif (data["question type"] == "grey character power"):
                    data["game state"]["shadow"] = data["data"][choice]
                    data["chosen character"]["usingPower"] = False

            # blue character
            if data["chosen character"]["color"] == "blue":
                available_rooms = [room for room in range(10)]
                if (data["question type"].startswith("activate") == True):
                    data["question type"] = "blue character power room"
                    data["data"] = available_rooms
                elif (data["question type"] == "blue character power room"):
                    data["game state"]["blocked"][0] = data["data"][choice]
                    available_exits = list(self.passages[data["data"][choice]])
                    data["question type"] = "blue character power exit"
                    data["data"] = available_exits
                elif (data["question type"] == "blue character power exit"):
                    data["game state"]["blocked"][1] = data["data"][choice]
                    data["chosen character"]["usingPower"] = False

        # set question and choice for moving character
        if data["chosen character"]["canMove"] == True and data["chosen character"]["usingPower"] == False:
            data["question type"] = "select position"
            pass_act = self.pink_passages if data["chosen character"]["color"] == 'pink' else self.passages
            disp = {x for x in pass_act[data["chosen character"]["position"]]
                if data["chosen character"]["position"] not in data["game state"]["blocked"] or x not in data["game state"]["blocked"]}
                #POUR moi c'est censé etre un and...mais bon -.o
            data["data"] = list(disp)

        # set question and choice for select character
        if data["chosen character"]["canMove"] == False and data["chosen character"]["usingPower"] == False:
            data["question type"] = "select character"
            data["data"] = data["game state"]["active character_cards"]

        return data
