
def heuristic(data, isInvestigator):

    if (isInvestigator == False):
        fantom_will_scream = True
        suspect_may_scream = 0
        suspect_may_not_scream = 0
        fantom_color = data["game state"]["fantom"]
        fantom_position = 0
        for i in range(10):
            for q in data["game state"]["characters"]:
                if q["position"] == i and q["suspect"] == True:
                    is_alone = True
                    for c in data["game state"]["characters"]:
                        if c["position"] == i and c["color"] != q["color"]:
                            is_alone = False
                            if q["color"] == fantom_color:
                                fantom_will_scream = False
                    if is_alone == True or q["position"] == data["game state"]["shadow"]:
                        suspect_may_scream += 1
                    else:
                        suspect_may_not_scream += 1
        # print ("May scream :", suspect_may_scream)
        # print ("May  not scream :", suspect_may_not_scream)
        if (fantom_will_scream):
            return suspect_may_scream
        else:
            return suspect_may_not_scream

    else:
        min_suspect_check = 0
        suspect_may_scream = 0
        suspect_may_not_scream = 0
        for i in range(10):
            for q in data["game state"]["characters"]:
                if q["position"] == i and q["suspect"] == True:
                    is_alone = True
                    for c in data["game state"]["characters"]:
                        if c["position"] == i and c["color"] != q["color"]:
                            is_alone = False
                    if is_alone == True or q["position"] == data["game state"]["shadow"]:
                        suspect_may_scream += 1
                    else:
                        suspect_may_not_scream += 1
        if (suspect_may_scream > suspect_may_not_scream):
            min_suspect_check = suspect_may_not_scream
        else:
            min_suspect_check = suspect_may_scream

    # print ("May scream :", suspect_may_scream)
    # print ("May  not scream :", suspect_may_not_scream)
    return min_suspect_check

#data = dict()
#data = {'question type': 'activate blue power', 'data': [1], 'game state': {'position_carlotta': 4, 'exit': 22, 'num_tour': 1, 'shadow': 3, 'blocked': [3, 7], 'characters': [{'color': 'black', 'suspect': True, 'position': 4, 'power': True}, {'color': 'grey', 'suspect': True, 'position': 4, 'power': True}, {'color': 'blue', 'suspect': True, 'position': 4, 'power': False}, {'color': 'brown', 'suspect': True, 'position': 4, 'power': True}, {'color': 'red', 'suspect': True, 'position': 4, 'power': True}, {'color': 'pink', 'suspect': True, 'position': 1, 'power': True}, {'color': 'white', 'suspect': True, 'position': 2, 'power': True}, {'color': 'purple', 'suspect': True, 'position': 4, 'power': True}], 'character_cards': [{'color': 'grey', 'suspect': True, 'position': 4, 'power': True}, {'color': 'brown', 'suspect': True, 'position': 1, 'power': True}, {'color': 'purple', 'suspect': True, 'position': 0, 'power': True}, {'color': 'blue', 'suspect': True, 'position': 5, 'power': False}, {'color': 'red', 'suspect': True, 'position': 2, 'power': True}, {'color': 'black', 'suspect': True, 'position': 7, 'power': True}, {'color': 'pink', 'suspect': True, 'position': 3, 'power': True}, {'color': 'white', 'suspect': True, 'position': 5, 'power': True}], 'active character_cards': [{'color': 'grey', 'suspect': True, 'position': 4, 'power': True}, {'color': 'purple', 'suspect': True, 'position': 0, 'power': True}], 'fantom': 'grey'}}
#r = heuristic(data, False)
#print(r)
