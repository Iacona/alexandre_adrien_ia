from clientSrc.Node import Node
from clientSrc.heuristic import heuristic

class MinimaxABAgent:

    def __init__(self, max_depth):
        """
        Initiation
        Parameters
        ----------
        max_depth : int
            The max depth of the tree
        """

    def minimax(self, node, isMaximizing, alpha, beta, isInvestigator):
        # si c'est la derniere node je return la valeur de l'heristique
        if(len(node.childs) == 0):
            truc = heuristic(node.data, isInvestigator) # IMPORTANT appeller la fonction d'Adrien
            return 0, truc
        # si je maximise
        if (isMaximizing):
            bestMove = -9999
            bestIndex = 0
            # pour chaque nodes filles
            for num, x in enumerate(node.childs, start=0):
                if x == None:
                    value = -10000
                else:
                    index, value = self.minimax( x, self.calculTurn(x.data, isMaximizing), alpha, beta, isInvestigator)
                if bestMove < value:
                    bestIndex = num
                bestMove = max(bestMove, value)

                alpha = max(alpha, bestMove)
                if beta <= alpha:
                    return num, bestMove
            return bestIndex, bestMove
        # si je minimise
        else:
            bestMove = 9999
            bestIndex = 0
            # pour chaque nodes filles
            for num, x in enumerate(node.childs, start=0):
                if x == None:
                    value = 10000
                else:
                    index, value = self.minimax( x, self.calculTurn(x.data, isMaximizing), alpha, beta, isInvestigator)
                if bestMove > value:
                    bestIndex = num
                bestMove = min(bestMove, value)
                alpha = min(alpha, bestMove)
                if beta < alpha:
                    return num, bestMove
            return 0, bestMove

    def calculTurn(self, data, isMaximizing):
        # calculate if it's maximiser or minimiser turn
        if (data["question type"] == "select character"):
            if (len(data["game state"]["active character_cards"]) == 3 or
                len(data["game state"]["active character_cards"]) == 1):
                return not isMaximizing
        return isMaximizing

    def core(self, data, isInvestigator, game):
        # create tree
        if data["question type"] == "select character":
            node = Node()
            node.makeTree(data, game)
        else:
            node = game.node
        # call minimax with tree
        index, value = self.minimax(node, True, -10000, 10000, isInvestigator)
        if (len(node.childs) != 0):
            game.chosenCharacter = node.childs[index].data["chosen character"]
            game.node = node.childs[index]
        return index
