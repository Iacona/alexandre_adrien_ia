import copy

class Node : 

    def __init__(self):
        """
        Initiation
        Parameters
        ----------
        childs  : tab[Node]
            all nodes linked to this node
        sate    : tab[string]
            current state for this node
        """
        self.childs = []
        self.data = dict

    def recurse_nodes(self, data, game):
        #print(" data :\n {}".format(data))
        self.data = data
        if (len(data["game state"]["active character_cards"]) == 0 
            and data["question type"] == "select character"):
            return
        for i in range(len(self.data["data"])):
            if (self.data["question type"] == "blue character power exit" and self.data["game state"]["blocked"][0] > self.data["data"][i]):
                self.childs.append(None)
            else:
                newNode = Node()
                self.childs.append(newNode)
                self.childs[-1].recurse_nodes(game.calculateNewData(copy.deepcopy(self.data), i), game)

    def makeTree(self, data, game):
        if data["question type"] == "select character":
            data["chosen character"] = {"color": "", "position": -1, "canMove": True, "usingPower": False, "alreadyUsePower": False}
        else:
            data["chosen character"] = game.chosenCharacter
        self.recurse_nodes(data, game)


# THIS is for testing creation of the tree, tun src/Node.py
#
# from Game import Game
# 
# data = dict()
# game = Game()
# game.chosenCharacter = {'color': 'red', 'position': 5, 'canMove': False, 'usingPower': False, 'alreadyUsePower': False}
# data = {'question type': 'select character', 'data': [{'color': 'brown', 'suspect': True, 'position': 7, 'power': True}, {'color': 'red', 'suspect': True, 'position': 6, 'power': True}, {'color': 'blue', 'suspect': True, 'position': 4, 'power': True}, {'color': 'purple', 'suspect': True, 'position': 0, 'power': True}], 'game state': {'position_carlotta': 4, 'exit': 22, 'num_tour': 1, 'shadow': 4, 'blocked': [6, 7], 'characters': [{'color': 'black', 'suspect': True, 'position': 7, 'power': True}, {'color': 'grey', 'suspect': True, 'position': 7, 'power': True}, {'color': 'blue', 'suspect': True, 'position': 5, 'power': False}, {'color': 'brown', 'suspect': True, 'position': 7, 'power': True}, {'color': 'red', 'suspect': True, 'position': 6, 'power': True}, {'color': 'pink', 'suspect': True, 'position': 3, 'power': True}, {'color': 'white', 'suspect': True, 'position': 7, 'power': True}, {'color': 'purple', 'suspect': True, 'position': 0, 'power': True}], 'character_cards': [{'color': 'grey', 'suspect': True, 'position': 4, 'power': True}, {'color': 'brown', 'suspect': True, 'position': 1, 'power': True}, {'color': 'purple', 'suspect': True, 'position': 0, 'power': True}, {'color': 'blue', 'suspect': True, 'position': 5, 'power': False}, {'color': 'red', 'suspect': True, 'position': 2, 'power': True}, {'color': 'black', 'suspect': True, 'position': 9, 'power': True}, {'color': 'pink', 'suspect': True, 'position': 3, 'power': True}, {'color': 'white', 'suspect': True, 'position': 5, 'power': True}], 'active character_cards': [{'color': 'brown', 'suspect': True, 'position': 7, 'power': True}, {'color': 'red', 'suspect': True, 'position': 6, 'power': True}, {'color': 'blue', 'suspect': True, 'position': 4, 'power': True}, {'color': 'purple', 'suspect': True, 'position': 0, 'power': True}], 'fantom': 'grey'}}
# node = Node()
# node.makeTree(data, game)